package com.example.vaadindemo.domain;

import java.util.Date;
import java.util.UUID;
 
public class Film {
       
        private UUID id;
       
        private String tytul;
        private String opis;
        private String rezyser;
        private int rokProdukcji;
        private String kanal;
        private String godzina;
        
        public Film(String tytul, String opis, int rokProdukcji, String rezyser, String kanal, String godzina) {
                super();
                this.tytul = tytul;
                this.opis = opis;
                this.rokProdukcji = rokProdukcji;
                this.rezyser = rezyser;
                this.kanal = kanal;
                this.godzina = godzina;
        }
       
        public String getGodzina() {
			return godzina;
		}

		public void setGodzina(String godzina) {
			this.godzina = godzina;
		}

		public String getKanal() {
			return kanal;
		}

		public void setKanal(String kanal) {
			this.kanal = kanal;
		}

		public int getRokProdukcji() {
			return rokProdukcji;
		}

		public void setRokProdukcji(int rokProdukcji) {
			this.rokProdukcji = rokProdukcji;
		}

		public UUID getId() {
                return id;
        }
        public void setId(UUID id) {
                this.id = id;
        }
        public String getTytul() {
                return tytul;
        }
        public void setTytul(String tytul) {
                this.tytul = tytul;
        }
        public String getOpis() {
                return opis;
        }
        public void setOpis(String opis) {
                this.opis = opis;
        }
        public String getRezyser() {
                return rezyser;
        }
        public void setRezyser(String rezyser) {
                this.rezyser = rezyser;
        }
       
       
}